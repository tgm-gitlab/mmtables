import System.Environment
import Numeric(showFFloat)

data Value = Valid Double | Invalid
  deriving (Show, Read)

data Table =  Metric Double Double Double Double |
              Imperial Double Double Double Double |
              Formatted String String String String |
              Size Double Double Double Double Double Double Double |
              FormattedSize String String String String String String String |
              Absent |
              InvalidArgs |
              OutOfBounds
  deriving (Show, Read)

up :: Table -> Table
up (Metric m t d v) =  (Metric (m * 2) (t * 2) (d * 2) (v * 2))
up (Imperial m t d v) =  (Imperial (m * 2) (t * 2) (d * 2) (v * 2))
up _ = InvalidArgs

down :: Table -> Table
down (Metric m t d v) =  (Metric (m / 2) (t / 2) (d / 2) (v / 2))
down (Imperial m t d v) =  (Imperial (m / 2) (t / 2) (d / 2) (v / 2))
down _ = InvalidArgs

imperial :: Value -> Table
imperial (Valid (-4)) = (Imperial 3 0.25 12 0.0625)
imperial (Valid (-2)) = (Imperial 12 1 72 0.25)
imperial (Valid 0) = (Imperial 50 6 360 1) -- 21 edge cases
imperial (Valid 2) = (Imperial 200 30 1440 4)
imperial (Valid 3) = (Imperial 400 60 3000 8)
imperial (Valid 4) = (Imperial 800 120 6000 15)
imperial (Valid 5) = (Imperial 1600 240 10800 30)
imperial (Valid 7) = (Imperial 6000 900 31680 125)
imperial (Valid 10) = (Imperial 50000 7200 253440 1000)
imperial (Valid 13) = (Imperial 400000 57600 1900800 8000)
imperial (Valid 14) = (Imperial 800000 86400 3801600 15000)
imperial (Valid 15) = (Imperial 1600000 172800 7603200 32000)
imperial (Valid 16) = (Imperial 3200000 345600 15840000 65000)
imperial (Valid 17) = (Imperial 6400000 604800 31680000 125000)
imperial (Valid 18) = (Imperial 12000000 1209600 63360000 250000)
imperial (Valid 19) = (Imperial 24000000 2629800 126720000 500000)
imperial (Valid 20) = (Imperial 50000000 5259600 253440000 1000000)
imperial (Valid 23) = (Imperial 400000000 47336400 2027520000 8000000)
imperial (Valid 24) = (Imperial 800000000 94672800 4055040000 15000000)
imperial (Valid 25) = (Imperial 1600000000 189345600 7920000000 32000000)
imperial (Valid 26) = (Imperial 3200000000 378691200 15840000000 65000000)
imperial (Valid 27) = (Imperial 6400000000 788940000 31680000000 125000000)
imperial (Valid 29) = (Imperial 25000000000 3155760000 126720000000 250000000)
imperial (Valid x)
  | x > 0 = up (imperial (Valid (x - 1)))
  | x < -5 = Absent
  | x < 0 = down (imperial (Valid (x + 1)))
imperial Invalid = InvalidArgs
imperial (Valid _) = InvalidArgs

metric :: Value -> Table
metric (Valid (-5)) = (Metric 750 0.125 15 0.0008)
metric (Valid (-4)) = (Metric 1500 0.25 50 0.0017)
metric (Valid (-3)) = (Metric 3000 0.5 100 0.0035)
metric (Valid (-2)) = (Metric 6000 1 200 0.007)
metric (Valid (-1)) = (Metric 12000 3 400 0.014)
metric (Valid 0) = (Metric 24000 6 800 0.025) -- 22 edge cases
metric (Valid 1) = (Metric 50000 12 1600 0.05)
metric (Valid 2) = (Metric 100000 30 3200 0.1)
metric (Valid 4) = (Metric 400000 120 12500 0.4)
metric (Valid 6) = (Metric 1600000 480 50000 1.7)
metric (Valid 7) = (Metric 3200000 900 100000 3.5)
metric (Valid 8) = (Metric 6000000 1800 200000 7)
metric (Valid 9) = (Metric 12000000 3600 400000 15)
metric (Valid 10) = (Metric 25000000 7200 800000 30)
metric (Valid 13) = (Metric 200000000 57600 6400000 250)
metric (Valid 14) = (Metric 400000000 86400 12500000 500)
metric (Valid 17) = (Metric 3200000000 604800 100000000 4000)
metric (Valid 18) = (Metric 6000000000 1209600 200000000 8000)
metric (Valid 19) = (Metric 12000000000 2629800 400000000 15000)
metric (Valid 20) = (Metric 25000000000 5259600 800000000 30000)
metric (Valid 23) = (Metric 200000000000 47336400 6400000000 250000)
metric (Valid 24) = (Metric 400000000000 94672800 12500000000 500000)
metric (Valid 27) = (Metric 3200000000000 788940000 100000000000 4000000)
metric (Valid 29) = (Metric 12500000000000 3155760000 400000000000 15000000)
metric (Valid x)
  | x > 0 = up (metric (Valid (x - 1)))
  | x < -5 = Absent
  | x < 0 = down (metric (Valid (x + 1)))
metric Invalid = InvalidArgs
metric (Valid _) = InvalidArgs

size :: Value -> Table
size (Valid 3) = (Size 3000 (-10) (-20) 10 20 20 2)
size (Valid 2) = (Size 1440 (-8) (-16) 8 16 16 2)
size (Valid 1) = (Size 720 (-6) (-12) 6 12 12 1)
size (Valid 0) = (Size 360 (-4) (-8) 4 8 8 1)
size (Valid (-1)) = (Size 180 (-2) (-4) 2 4 4 0)
size (Valid (-2)) = (Size 72 0 0 0 0 0 0)
size (Valid (-3)) = (Size 36 2 4 (-2) (-1) 0 0)
size (Valid (-4)) = (Size 12 4 8 (-4) (-2) 0 (-1))
size (Valid (-5)) = (Size 6 6 12 (-6) (-3) 0 (-1))
size (Valid (-6)) = (Size 3 8 16 (-8) (-4) 0 (-2))
size (Valid (-7)) = (Size 1 10 20 (-10) (-5) 0 (-2))
size (Valid _) = OutOfBounds
size _ =  InvalidArgs

isNumber :: String -> Bool
isNumber str =
    case (reads str) :: [(Double, String)] of
      [(_, "")] -> True
      _         -> False

cast :: [String] -> Value
cast [str] = if isNumber (str) then (Valid (read str)) else Invalid
cast _ = Invalid

format :: Table -> Table
format (Metric m t d v) = (Formatted (mass m 'm') (time t) (distance d 'm') (volume v 'm'))
format (Imperial m t d v) = (Formatted (mass m 'i') (time t) (distance d 'i') (volume v 'i'))
format InvalidArgs = InvalidArgs
format (Size h ad st i str sta spd) = (FormattedSize (distance h 'i') (show ad) (show st) (show i) (show str) (show sta) (show spd))
format formatted = formatted

massUnit :: String -> Double
massUnit "kt"   = 1000000000
massUnit "t"    = 1000000
massUnit "kg"   = 1000
massUnit "kton" = 2000000
massUnit "ton"  = 2000
massUnit _      = 0

showFullPrecision :: Double -> String
showFullPrecision x = showFFloat Nothing x ""

convert :: Double -> Double -> String
convert value unit = showFullPrecision (value / unit)


mass :: Double -> Char -> String
mass x 'm'
          | x >= (massUnit "kt") = ((convert x (massUnit "kt")) ++ " kt")
          | x >= (massUnit "t")  = ((convert x (massUnit "t")) ++ " t")
          | x >= (massUnit "kg") = ((convert x (massUnit "kg")) ++ " kg")
          | otherwise = ((showFullPrecision x) ++ " g")
mass x 'i'
          | x >= (massUnit "kton") = ((convert x (massUnit "kton")) ++ " short ktons")
          | x >= (massUnit "ton") = ((convert x (massUnit "ton")) ++ " short tons")
          | otherwise = ((showFullPrecision x) ++ " lbs")
mass _ _ = ""

timeUnit :: String -> Double
timeUnit "year" = 31557600
timeUnit "month" = 2629800
timeUnit "week" = 604800
timeUnit "day" = 86400
timeUnit "hour" = 3600
timeUnit "minute" = 60
timeUnit _ = 0

time :: Double -> String
time x
      | x >= (timeUnit "year") = ((convert x (timeUnit "year")) ++  " years")
      | x >= (timeUnit "month") = ((convert x (timeUnit "month")) ++ " months")
      | x >= (timeUnit "week") = ((convert x (timeUnit "week")) ++ " weeks")
      | x >= (timeUnit "day") = ((convert x (timeUnit "day")) ++ " days")
      | x >= (timeUnit "hour") =  ((convert x (timeUnit "hour")) ++ " hours")
      | x >= (timeUnit "minute") = ((convert x (timeUnit "minute")) ++ " minutes")
      | otherwise = ((showFullPrecision x) ++ " seconds")

distanceUnit :: String -> Double
distanceUnit "meter" = 100
distanceUnit "km" = 100000
distanceUnit "mkm" = 100000000000
distanceUnit "foot" = 12
distanceUnit "mile" = 63360
distanceUnit "mmile" = 63360000000
distanceUnit _ = 0

distance :: Double -> Char -> String
distance x 'm'
              | x >= (distanceUnit "mkm") = ((convert x (distanceUnit "mkm")) ++ " million km")
              | x >= (distanceUnit "km") = ((convert x (distanceUnit "km")) ++ " km")
              | x >= (distanceUnit "meter") = ((convert x (distanceUnit "meter")) ++ " meters")
              | otherwise = ((showFullPrecision x) ++ " cm")
distance x 'i'
              | x >= (distanceUnit "mmile") = ((convert x (distanceUnit "mmile")) ++ " million miles")
              | x >= (distanceUnit "mile") = ((convert x (distanceUnit "mile")) ++ " miles")
              | x >= (distanceUnit "foot") = ((convert x (distanceUnit "foot")) ++ " feet")
              | otherwise = ((showFullPrecision x) ++ " inches")
distance _ _ = ""

volumeUnit :: String -> Double
volumeUnit "m" = 1000000
volumeUnit _ = 0

volume :: Double -> Char -> String
volume x 'm'
            | x >= (volumeUnit "m") = ((convert x (volumeUnit "m")) ++ " million m³")
            | otherwise = ((showFullPrecision x) ++ " m³")
volume x 'i'
            | x >= (volumeUnit "m") = ((convert x (volumeUnit "m")) ++ " million cft")
            | otherwise = ((showFullPrecision x) ++ " cft.")
volume _ _ = ""

draw :: Table -> IO()
draw InvalidArgs = putStrLn "Supplied arguments are invalid."
draw OutOfBounds = putStrLn "Arguements are out of bounds."
draw Absent = putStrLn "Ranks below -5 count as absent and automatically fail."
draw (Formatted m t d v) = putStrLn ("\n" ++
                                     "Mass:\t\t\t" ++ m ++ "\n" ++
                                     "Time:\t\t\t" ++ t ++ "\n" ++
                                     "Distance:\t\t" ++ d ++ "\n" ++
                                     "Volume:\t\t\t" ++ v ++ "\n")
draw (FormattedSize h ad st i str sta spd) = putStrLn ("\n" ++
                                                       "Height/Length:\t\t" ++ h ++ "\n" ++
                                                       "Active Defence:\t\t" ++ ad ++ "\n" ++
                                                       "Stealth:\t\t" ++ st ++ "\n" ++
                                                       "Intimidation:\t\t" ++ i ++ "\n" ++
                                                       "Strength:\t\t" ++ str ++ "\n" ++
                                                       "Stamina:\t\t" ++ sta ++ "\n" ++
                                                       "Speed:\t\t\t" ++ spd ++ "\n")
draw err = putStrLn ("Unexpected Error: " ++ (show err))

handle :: [String] -> IO()
handle ("-m":str) = draw (format (metric (cast str)))
handle ("-i":str) = draw (format (imperial (cast str)))
handle ("-s":str) = draw (format (size (cast str)))
handle _ = putStrLn "Invalid arguments"

main :: IO ()
main = do
  args <- getArgs
  handle args


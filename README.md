# mmmeasure

A small Haskell program to calculate mass, time, distance and volume ranks from Mutants and Masterminds. Supports metric and imperial tables.